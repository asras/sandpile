#ifndef RECT_H
#define RECT_H
#include "raylib.h"


Rectangle rect_left(Rectangle rect, float relative_width);
Rectangle rect_right(Rectangle rect, float relative_width);
Rectangle rect_bottom(Rectangle rect, float relative_height);
Rectangle rect_top_right(Rectangle rect, float relative_width, float relative_height);
Rectangle rect_top_left(Rectangle rect, float relative_width, float relative_height);
Rectangle rect_bottom_left_margin(Rectangle rect, float relative_width, float relative_height, float relative_margin_left, float relative_margin_bottom);
Rectangle rect_above_similar(Rectangle rect, float relative_spacing);
Rectangle rect_above(Rectangle rect, float relative_height, float relative_spacing);
Rectangle rect_below_similar(Rectangle rect, float relative_spacing);
Rectangle rect_center(Rectangle rect, float relative_width, float relative_height);


#ifdef RECT_IMPLEMENTATION
Rectangle rect_left(Rectangle rect, float relative_width) {
    return (Rectangle) {
        rect.x,
        rect.y,
        rect.width * relative_width,
        rect.height
    };
}
Rectangle rect_right(Rectangle rect, float relative_width) {
    return (Rectangle) {
        rect.x + rect.width - rect.width * relative_width,
        rect.y,
        rect.width * relative_width,
        rect.height
    };
}
Rectangle rect_bottom(Rectangle rect, float relative_height) {
    return (Rectangle) {
        rect.x,
        rect.y + rect.height - rect.height * relative_height,
        rect.width,
        rect.height * relative_height
    };
}

Rectangle rect_top_right(Rectangle rect, float relative_width, float relative_height) {
    return (Rectangle) {
        rect.x + rect.width - rect.width * relative_width,
        rect.y,
        rect.width * relative_width,
        rect.height * relative_height
    };
}
Rectangle rect_top_left(Rectangle rect, float relative_width, float relative_height) {
    return (Rectangle) {
        rect.x,
        rect.y,
        rect.width * relative_width,
        rect.height * relative_height
    };
}
Rectangle rect_bottom_left_margin(Rectangle rect, float relative_width, float relative_height, float relative_margin_left, float relative_margin_bottom) {
    return (Rectangle) {
        rect.x + rect.width * relative_margin_left,
        rect.y + rect.height - rect.height * relative_height - rect.height * relative_margin_bottom,
        rect.width * relative_width,
        rect.height * relative_height
    };
}
Rectangle rect_above_similar(Rectangle rect, float relative_spacing) {
    return (Rectangle) {
        rect.x,
        rect.y - rect.height * (1.0f + relative_spacing),
        rect.width,
        rect.height
    };
}
Rectangle rect_above(Rectangle rect, float relative_height, float relative_spacing) {
    return (Rectangle) {
        rect.x,
        rect.y - rect.height * (relative_height + relative_spacing),
        rect.width,
        rect.height * relative_height
    };
}
Rectangle rect_below_similar(Rectangle rect, float relative_spacing) {
    return (Rectangle) {
        rect.x,
        rect.y + rect.height * (1.0f + relative_spacing),
        rect.width,
        rect.height
    };
}

Rectangle rect_center(Rectangle rect, float relative_width, float relative_height) {
    return (Rectangle) {
        rect.x + rect.width * (1.0f - relative_width) / 2,
        rect.y + rect.height * (1.0f - relative_height) / 2,
        rect.width * relative_width,
        rect.height * relative_height
    };
}

#endif // RECT_IMPLEMENTATION
#endif // !RECT_H
